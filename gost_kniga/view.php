<?php
class view
{
    private $pageNumber;
    private $pageMessages;
    private $pagesAmount;
    private $paginationIndent;
    private $paginator;

    public function __construct($pageMessages, $pagesAmount)
    {
        $this->pageMessages = $pageMessages;
        $this->pagesAmount = $pagesAmount;
        $this->paginationIndent = 1;
        $this->pageNumber = 1;
    }

    public function setIndentNumber($paginationIndent, $pageNumber)
    {
        $this->paginationIndent = $paginationIndent;
        $this->pageNumber = $pageNumber;
    }

    public function header()
    {
        echo "<html><title>Гостевая книга</title><link rel='stylesheet' type='text/css'  href='style.css'/> <body><h1>Моя гостевая книга</h1><br></head>
            <div><a href='#'><h3>Оставьте ваш отзыв!</h3></a></div>";
    }

    public function footer()
    {
        echo "<br>";
        echo "<footer><h3>Янчевский В.И.</h3></footer></body></html>";
    }

    public function viewForm()
    {
        echo "<form method=\"POST\" action=\"kniga.php\">";
        echo "<a href name=\"regName\"></a><br>Ваше имя<br>";
        echo "<input type=\"text\" size=\"30\" maxlength=\"30\" name=\"regname\" value=\"\"><br>";
        echo "<br>Ваш отзыв<br>";
        echo "<textarea name=\"info\" cols=\"35\" rows=\"4\"></textarea><br>";
        echo "<input type=\"submit\" value=\"Отправить\" name=\"sendData\">";
        echo "<input type=\"reset\" value=\"Отмена\">";
        echo "</form>";
    }

    public function run()
    {
        $this->header();
        $this->viewPage();
        $this->viewForm();
        $this->footer();
        $this->paginator = new paginator($this->pageNumber, $this->pagesAmount, $this->paginationIndent);
        $this->paginator->run();
    }

    public function viewPage()
    {
        echo "<br>";
        foreach ($this->pageMessages as $value)
            echo "$value<br>";
    }
}


?>