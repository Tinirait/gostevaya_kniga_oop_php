<?php
class Controller {
    private $model;//объект модель
    private $view;//объект вид
    private $pageNumber;//номер запрошенной страницы

    public function __construct() {//конструктор определяет номер запрошенной страницы
        $this->pageNumber = isset($_GET['number'])?intval($_GET['number']):1;
    }
    public function run() {
        try
        {
            if ($_POST['sendData']&&(trim($_POST['regname'])!='')&&(trim($_POST['info'])!=''))
                $this->model = new Model(1);//создаем новую модель
            else $this->model = new Model(0);
            $this->model->setMessagesPerPage(MESSAGES_PER_PAGE);//передаем ей количество сообщений на странице
            $this->model->run($this->pageNumber);//запускаем, она извлекает нужные данные
            $this->view = new view($this->model->getPageMessages(),$this->model->getPagesAmount());//создаем новый объект View
            $this->view->setIndentNumber(PAGINATION_INDENT,$this->pageNumber); //выводим страницу
            $this->view->run();
        }
        catch(Exception $e) {
            echo 'Ошибка!<br/>' . $e->getMessage();
        }

    }
}
?>