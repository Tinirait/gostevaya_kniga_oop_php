<?php
class addFacts
{
    private $nameUser;
    private $textUser;
    private $material;
    private $dataUser;

    public function __construct($nameUser,$textUser)
    {
        $this->nameUser=trim(strip_tags($nameUser));
        $this->textUser=trim(strip_tags($textUser));
    }

    public function Data()
    {
        $this->dataUser="<div><p>$this->nameUser написал(а):</p>";
        $this->dataUser.="<p>$this->textUser</p>";
        $this->dataUser.='<p>'.date('H:i l d F Y').'.</p></div>';
    }

    public function run()
    {
            $this->material=fopen('kniga.txt','a');
            $this->Data();
            fwrite($this->material, $this->dataUser);
            fclose($this->material);
    }
}
?>
