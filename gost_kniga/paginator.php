<?php
class paginator{
    private $pageNumber;//номер запрошенной страницы
    private $pagesAmount;//количество страниц
    private $paginationIndent;//количество номеров слева и справа от текущей страницы в пагинаторе
    private $firstPage;//первая страница
    private $lastPage;//последняя страница

    public function __construct($pageNumber,$pagesAmount,$paginationIndent){
        $this->pageNumber=$pageNumber;
        $this->pagesAmount=$pagesAmount;
        $this->paginationIndent=$paginationIndent;
    }

    public function getFirstPage(){
        ($this->pageNumber-$this->paginationIndent)<1?$this->firstPage=1:
            $this->firstPage=($this->pageNumber-$this->paginationIndent);
    }

    public function getLastPage(){
        (($this->pageNumber+$this->paginationIndent)>$this->pagesAmount)?($this->lastPage=$this->pagesAmount):
            ($this->lastPage=($this->pageNumber+$this->paginationIndent));
    }

    public function run(){
        $this->getFirstPage();
        $this->getLastPage();
        echo "<div>";
        for ($i=$this->firstPage;$i<=$this->lastPage;$i++)
            if ($i==$this->pageNumber)
                echo "$i ";
            else echo "<a href=kniga.php?number=$i>$i</a> ";
        echo "</div>";
    }
}
?>