<?php
class Model
{
    private $messagesArray;//массив сообщений
    private $messagesAmount;//общее количество сообщений
    private $messagesPerPage;//количество сообщений на одной странице
    private $pagesAmount;//количество страниц
    private $pageMessages;//массив сообщений для запрошенной страницы
    private $pageNumber;//номер запрошенной страницы
    private $addFacts;//работа с Пользователем


    public function __construct($switch)
    {
        if($switch==1){
            $this->addFacts=new addFacts($_POST['regname'],$_POST['info']);
            $this->addFacts->run();
        }
        $this->getMessagesArray();
        $this->messagesAmount = count($this->messagesArray);//определяем количество сообщений
        //устанавливаем для всех остальных свойств значения по умолчанию
        //чтобы объект был полностью инициализирован
        $this->messagesPerPage = 1;
        $this->pagesAmount = 1;
        $this->pageMessages = array();
        $this->pageNumber = 1;
    }

    private function getMessagesArray()
    {
        if (@file_get_contents('kniga.txt')===false)
        {
            throw new Exception('Файл базы данных не доступен');
        }
        $this->messagesArray = array_reverse(explode("<div>", file_get_contents('kniga.txt')));
    }

    public function setMessagesPerPage($messagesPerPage) { //установка количества сообщений на странице
        $this->messagesPerPage = $messagesPerPage;
    }

    public function run($pageNumber = 1) {//выполняет все действия, в аргументе передается номер запрошенной страницы
        $this->pageNumber = $pageNumber;//записываем полученный в параметре номер страницы в свойство
        $this->pagesAmount = ceil($this->messagesAmount/$this->messagesPerPage);//определяем количество страниц
        if ($pageNumber<1 || $pageNumber>$this->pagesAmount) {//если номер страницы вне диапазона существующих страниц
            throw new Exception('Запрошенная страница не существует');//генерируем исключение, оно перехватится в контроллере
        }
        $messageOffset = ($pageNumber-1) * $this->messagesPerPage;//номер первого сообщения на странице
        $this->pageMessages = array_slice($this->messagesArray, $messageOffset, $this->messagesPerPage);//извлекаем сообщения для страницы
    }

    public function getPageMessages() {//функция для получения сообщений страницы для передачи View
        return $this->pageMessages;
    }

    public function getPagesAmount() {//функция для получения количества страниц тоже для передачи View
        return $this->pagesAmount;
    }
}
?>